import { IServices } from 'infrastructure/services/IServices';
import { ILogger } from 'logger/Logger';

import { PaginatedResult } from '../../../entities/common/Pagination';
import { Task } from '../../../entities/task/Task';
import { CreateTaskDto } from '../../../entities/task/Task.dto';

type Payload = {};
export const getAllTask =
  ({ taskService }: IServices, logger: ILogger) =>
  async ({ limit, page, search, sort }: any): Promise<PaginatedResult<Task>> => {
    return await taskService.getAll({ limit, page, search, sort });
  };
