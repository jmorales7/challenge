import { PaginatedResult, PaginationParams } from '../common/Pagination';
import { Task } from './Task';
import { CreateTaskDto, UpdateTaskDto } from './Task.dto';

export type IRepositoryTask = {
  create(task: CreateTaskDto): Promise<Task>;
  getById(id: number): Promise<Task | null>;
  update(id: number, program: UpdateTaskDto): Promise<Task>;
  delete(id: string): Promise<void>;
  getAll(pagination: PaginationParams): Promise<PaginatedResult<Task>>;
};
