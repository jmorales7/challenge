import { User } from './User';
import { CreateUserDto } from './User.dto';

export type IRepositoryUser = {
  create(user: CreateUserDto): Promise<User>;
  validateSharedIds(shared: string[]): Promise<boolean>;
};
