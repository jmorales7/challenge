import { Sequelize } from 'sequelize-typescript';

import { ILogger } from '../../../logger/Logger';

export async function openConnection(mysql: any, logger: ILogger): Promise<void> {
  try {
    const sequelize = new Sequelize({
      database: mysql.database,
      username: mysql.username,
      password: mysql.password,
      host: mysql.host,
      dialect: mysql.dialect
    });
    await sequelize.authenticate();
    logger.info('Connection to mysql successfully opened!');
  } catch (err) {
    logger.error('Failed to open connection to mysql', err);
    throw err;
  }
}
