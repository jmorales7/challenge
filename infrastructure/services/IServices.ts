import { ITask } from '../../application/entities/task/ITask';
import { IUser } from '../../application/entities/user/IUser';

export type IServices = {
  taskService: ITask;
  userService: IUser;
};
