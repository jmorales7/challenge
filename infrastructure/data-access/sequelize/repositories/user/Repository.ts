import { CustomError, Errors } from '../../../../../application/entities/shared/Errors';
import { IRepositoryUser } from '../../../../../application/entities/user/User.repository';
import { ILogger } from '../../../../../logger/Logger';
import { UserModel, userDataParser } from './Schema';

export const userRepository = (logger: ILogger): IRepositoryUser => ({
  async create(user) {
    const log = logger.child({ function: 'create' });
    try {
      await UserModel.sync();
      return UserModel.create(user).then(userDataParser);
    } catch (e: any) {
      log.error(e);
      if (e instanceof CustomError) {
        throw new CustomError(e.code as Errors, e.message);
      }
      throw new CustomError(Errors.SERVER_ERROR, e);
    }
  },
  async validateSharedIds(shared) {
    const log = logger.child({ function: 'validateSharedIds' });
    try {
      const users = await UserModel.findAll({ where: { id: shared } });
      return users.length === shared.length;
    } catch (e: any) {
      log.error(e);
      if (e instanceof CustomError) {
        throw new CustomError(e.code as Errors, e.message);
      }
      throw new CustomError(Errors.SERVER_ERROR, e);
    }
  }
});
