import { CustomError, Errors } from '../../../../../application/entities/shared/Errors';
import { IRepositoryTask } from '../../../../../application/entities/task/Task.repository';
import { ILogger } from '../../../../../logger/Logger';
import { paginate } from '../../pagination/Pagination';
import { TaskModel, taskDataParser } from './Schema';

export const taskRepository = (logger: ILogger): IRepositoryTask => ({
  async create(task) {
    const log = logger.child({ function: 'create' });
    try {
      await TaskModel.sync();
      return TaskModel.create(task).then(taskDataParser);
    } catch (e: any) {
      log.error(e);
      if (e instanceof CustomError) {
        throw new CustomError(e.code as Errors, e.message);
      }
      throw new CustomError(Errors.SERVER_ERROR, e);
    }
  },

  async update(id, task) {
    const log = logger.child({ function: 'update' });
    try {
      await TaskModel.update(task, { where: { id } });
      const taskFind = await TaskModel.findByPk(id);
      if (!taskFind) throw new CustomError(Errors.NOT_FOUND, 'Task not Found');
      return taskFind;
    } catch (e: any) {
      console.log(e);
      log.error(e);
      if (e instanceof CustomError) {
        throw new CustomError(e.code as Errors, e.message);
      }
      throw new CustomError(Errors.SERVER_ERROR, e);
    }
  },

  async delete(id) {
    const log = logger.child({ function: 'delete' });
    try {
      await TaskModel.destroy({ where: { id } });
    } catch (e) {
      log.error(e);
      if (e instanceof CustomError) {
        throw new CustomError(e.code as Errors, e.message);
      }
      throw new CustomError(Errors.SERVER_ERROR, 'something went wrong');
    }
  },

  async getById(id) {
    const log = logger.child({ function: 'getById' });
    try {
      return await TaskModel.findByPk(id);
    } catch (e) {
      log.error(e);
      if (e instanceof CustomError) {
        throw new CustomError(e.code as Errors, e.message);
      }
      throw new CustomError(Errors.SERVER_ERROR, 'something went wrong');
    }
  },

  async getAll(paginationParams) {
    const log = logger.child({ function: 'getAll' });
    try {
      const columns = ['title'];
      return await paginate(TaskModel, paginationParams, columns);
    } catch (e: any) {
      log.error(e);
      if (e instanceof CustomError) {
        throw new CustomError(e.code as Errors, e.message);
      }
      throw new CustomError(Errors.SERVER_ERROR, e);
    }
  }
});
