import { IServices } from 'infrastructure/services/IServices';
import { ILogger } from 'logger/Logger';

import { Task } from '../../../entities/task/Task';
import { GetByIdTaskDto } from '../../../entities/task/Task.dto';

export const getByIdTask =
  ({ taskService }: IServices, logger: ILogger) =>
  async ({ id }: GetByIdTaskDto): Promise<Task> => {
    return await taskService.getById(id);
  };
