import { IDataAccess } from 'infrastructure/data-access/IDataAccess';
import { ILogger } from 'logger/Logger';

import { IUser } from '../../../application/entities/user/IUser';

export const userService = ({ userRepository }: IDataAccess, logger: ILogger): IUser => ({
  async create(user) {
    return await userRepository.create(user);
  },
  async validateSharedIds(shared) {
    return await userRepository.validateSharedIds(shared);
  }
});
