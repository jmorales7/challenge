import { Sequelize } from 'sequelize-typescript';

const sequelize: Sequelize = new Sequelize({
  database: process.env['MYSQL_DB'],
  host: process.env['MYSQL_HOST'],
  dialect: 'mysql',
  password: process.env['MYSQL_PASSWORD'],
  username: process.env['MYSQL_USERNAME']
});

export default sequelize;
