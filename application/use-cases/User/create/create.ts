import { IServices } from 'infrastructure/services/IServices';
import { ILogger } from 'logger/Logger';

import { User } from '../../../entities/user/User';
import { CreateUserDto } from '../../../entities/user/User.dto';

type Payload = {
  body: CreateUserDto;
};
export const createUser =
  ({ userService }: IServices, logger: ILogger) =>
  async ({ body }: Payload): Promise<User> => {
    return await userService.create(body);
  };
