import { IDataAccess } from 'infrastructure/data-access/IDataAccess';
import { ILogger } from 'logger/Logger';

import { CustomError, Errors } from '../../../application/entities/shared/Errors';
import { ITask } from '../../../application/entities/task/ITask';

export const taskService = ({ taskRepository }: IDataAccess, logger: ILogger): ITask => ({
  async create(task) {
    return await taskRepository.create(task);
  },

  async getById(id) {
    const task = await taskRepository.getById(id);
    if (!task) throw new CustomError(Errors.NOT_FOUND, 'Task not found');
    return task;
  },
  async update(id, task) {
    return await taskRepository.update(id, task);
  },
  async delete(id) {
    await taskRepository.delete(id);
  },
  async getAll({ page, search, limit, sort }) {
    return await taskRepository.getAll({ page: Number(page), search, limit: Number(limit), sort });
  }
});
