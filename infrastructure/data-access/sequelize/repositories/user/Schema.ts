import { DataTypes, Model } from 'sequelize';

import { User } from '../../../../../application/entities/user/User';
import sequelize from '../../Conection';

export interface UserInstance extends Model, User {}

export const UserModel = sequelize.define<UserInstance>(
  'User',
  {
    id: {
      type: DataTypes.BIGINT,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    }
  },
  {
    timestamps: true,
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  }
);

export const userDataParser = (user: UserInstance): User => ({
  id: user.id,
  name: user.name,
  createdAt: user.createdAt,
  updatedAt: user.updatedAt
});
