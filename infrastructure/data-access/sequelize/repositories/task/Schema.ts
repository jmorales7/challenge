import { DataTypes, Model } from 'sequelize';

import { PaginatedResult } from '../../../../../application/entities/common/Pagination';
import { Status, Task } from '../../../../../application/entities/task/Task';
import sequelize from '../../Conection';

export interface TaskInstance extends Model, Task {}

export const TaskModel = sequelize.define<TaskInstance>(
  'Task',
  {
    id: {
      type: DataTypes.BIGINT,
      autoIncrement: true,
      primaryKey: true
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false
    },
    status: {
      type: DataTypes.ENUM('DONE', 'TO_DO'),
      allowNull: false
    },
    dateToFinish: {
      type: DataTypes.DATE,
      allowNull: false
    },
    isPublic: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    shared: {
      type: DataTypes.JSON // Definición del atributo como un arreglo de strings
    },
    comment: {
      type: DataTypes.STRING
    },
    createdBy: {
      type: DataTypes.STRING,
      allowNull: false
    },
    responsible: {
      type: DataTypes.STRING
    },
    tags: {
      type: DataTypes.STRING
    },
    file: {
      type: DataTypes.STRING
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    }
  },
  {
    timestamps: true,
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  }
);

export const taskDataParser = (task: TaskInstance): Task => ({
  id: task.id,
  title: task.title,
  description: task.description,
  status: task.status,
  dateToFinish: task.dateToFinish,
  isPublic: task.isPublic,
  shared: task.shared,
  comment: task.comment,
  createdBy: task.createdBy,
  tags: task.createdBy,
  createdAt: task.createdAt,
  updatedAt: task.updatedAt
});
