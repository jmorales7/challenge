import { z } from 'zod';

export interface Task {
  id: string;
  title: string;
  description: string;
  status: Status;
  dateToFinish: string;
  isPublic: boolean;
  shared?: string;
  comment?: string;
  createdBy: string;
  responsible?: string;
  tags?: string;
  file?: string;
  updatedAt: string;
  createdAt: string;
}

export const Status = z.nativeEnum({
  DONE: 'DONE',
  TO_DO: 'TO_DO'
} as const);

export type Status = z.infer<typeof Status>;
