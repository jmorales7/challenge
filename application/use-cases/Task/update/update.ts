import { IServices } from 'infrastructure/services/IServices';
import { ILogger } from 'logger/Logger';

import { CustomError, Errors } from '../../../entities/shared/Errors';
import { Task } from '../../../entities/task/Task';
import { UpdateTaskDto } from '../../../entities/task/Task.dto';

type Payload = {
  body: UpdateTaskDto;
  id: any;
  files: any;
};
export const updateTask =
  ({ taskService }: IServices, logger: ILogger) =>
  async ({ body, id, files }: Payload): Promise<Task> => {
    const task = await taskService.getById(id);
    console.log(task.shared, JSON.parse(task.shared!).includes(body.userId), task.createdBy != body.userId);
    if (task.shared && !JSON.parse(task.shared).includes(body.userId) && task.createdBy != body.userId)
      throw new CustomError(Errors.BAD_CREDENTIALS, 'This user can not edit');

    if (files) {
      const file = files.find((f: any) => f.fieldname === 'file');
      const allowedExtensions = ['application/pdf', 'image/jpeg', 'image/png'];
      const fileExtension = file.mimetype;

      if (!allowedExtensions.includes(fileExtension))
        throw new CustomError(Errors.INVALID_PARAMETERS, `File ${fileExtension} is not allowed `);
      const fileName = file.originalname;
      //add logic to store in AWS S3 Or SFTP remove the older file an add the new
      return await taskService.update(id, { file: fileName, ...body });
    }

    return await taskService.update(id, body);
  };
