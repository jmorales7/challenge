import { IConfig } from 'config/Type';

import { ILogger } from '../../logger/Logger';
import { IDataAccess } from './IDataAccess';
import { openConnection } from './sequelize/Db';
import { taskRepository } from './sequelize/repositories/task/Repository';
import { userRepository } from './sequelize/repositories/user/Repository';

export const dataAccess = async ({ mysql }: IConfig, loggerFactory: ILogger): Promise<IDataAccess> => {
  const logger = loggerFactory.child({ module: 'dataAccess' });
  try {
    openConnection(mysql, loggerFactory.child({ dataAccess: 'Mysql' })).then();
    const taskRepositorySetUp = taskRepository(logger.child({ repository: 'task' }));
    const userRepositorySetUp = userRepository(logger.child({ repository: 'user' }));
    //preInjection ? logger.info('Dependencies pre-injection done') : logger.info('Dependencies fully injected');
    return {
      taskRepository: taskRepositorySetUp,
      userRepository: userRepositorySetUp
    };
  } catch (e) {
    logger.error('Failed to initialize Data access with error:', e);
    throw e;
  }
};
