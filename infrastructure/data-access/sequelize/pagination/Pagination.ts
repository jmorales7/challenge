import { Op } from 'sequelize';
import { WhereOptions } from 'sequelize/types';

import { PaginatedResult, PaginationParams } from '../../../../application/entities/common/Pagination';

function buildSearchQuery(search: string, columns: string[]): WhereOptions {
  return {
    [Op.or]: columns.map((column) => ({
      [column]: {
        [Op.like]: `%${search}%`
      }
    }))
  };
}
export async function paginate<T>(
  model: any,
  options: PaginationParams,
  columns: string[]
): Promise<PaginatedResult<T>> {
  const { page, limit, search, sort } = options;
  const offset = (page - 1) * limit;

  const whereClause = search ? buildSearchQuery(search, columns) : {};

  const totalCount = await model.count({ where: whereClause });
  const totalPages = Math.ceil(totalCount / limit);

  const results = await model.findAll({
    where: whereClause,
    offset,
    limit,
    order: sort ? [sort.split(':')] : []
  });

  return {
    page,
    pages: totalPages,
    total: totalCount,
    limit,
    results
  };
}
