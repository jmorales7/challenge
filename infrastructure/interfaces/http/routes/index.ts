import { AnyZodObject } from 'zod';

import {
  CreateTaskDto,
  GetAllTaskDto,
  GetByIdTaskDto,
  UpdateTaskDto
} from '../../../../application/entities/task/Task.dto';
import { CreateUserDto } from '../../../../application/entities/user/User.dto';

export type Route = {
  path: string;
  verb: string;
  useCase: any;
  successCode?: number;
  fileBuffer?: boolean;
  schemaValidation?: AnyZodObject | undefined;
};

export const routes: (dependencies: any) => Array<Route> = (dependencies: any) => [
  //User
  { path: '/users', verb: 'POST', useCase: dependencies.createUser, schemaValidation: CreateUserDto },

  //Task
  { path: '/tasks', verb: 'POST', useCase: dependencies.createTask, multipart: true, schemaValidation: CreateTaskDto },
  { path: '/tasks/:id', verb: 'GET', useCase: dependencies.getByIdTask },
  { path: '/tasks/:id', verb: 'DELETE', useCase: dependencies.deleteTask },
  { path: '/tasks', verb: 'GET', useCase: dependencies.getAllTask, schemaValidation: GetAllTaskDto },
  {
    path: '/tasks/:id',
    verb: 'PUT',
    useCase: dependencies.updateTask,
    multipart: true,
    schemaValidation: UpdateTaskDto
  }
];
