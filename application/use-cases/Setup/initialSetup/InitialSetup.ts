import { IDataAccess } from 'infrastructure/data-access/IDataAccess';
import { IServices } from 'infrastructure/services/IServices';

import { ILogger } from '../../../../logger/Logger';
import { InitialSetupError } from './Errors';

export type InitConfig = {};

export const initialSetup =
  ({ taskService }: IServices, dataAccess: IDataAccess, logger: ILogger) =>
  async (): Promise<any> => {
    try {
      console.log('TODO');
    } catch (error) {
      logger.error(error);
      throw new InitialSetupError(error);
    }
  };
