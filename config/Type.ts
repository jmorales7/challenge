import Joi from 'joi';

import { ILogger } from '../logger/Logger';

export type IConfig = {
  http: {
    port: number;
  };
  baseLogger: ILogger;
  mysql: {
    database: string;
    username: string;
    password: string;
    host: string;
    dialect: string;
  };
};
