import { IServices } from 'infrastructure/services/IServices';

import { ILogger } from '../../logger/Logger';
import { createTask } from './Task/create/create';
import { deleteTask } from './Task/delete/delete';
import { getAllTask } from './Task/getAll/getAll';
import { getByIdTask } from './Task/getById/getById';
import { updateTask } from './Task/update/update';
import { createUser } from './User/create/create';

export const useCaseFactory = (services: IServices, baseLogger: ILogger) => ({
  createTask: createTask(services, baseLogger.child({ controller: 'createTask' })),
  deleteTask: deleteTask(services, baseLogger.child({ controller: 'deleteTask' })),
  getByIdTask: getByIdTask(services, baseLogger.child({ controller: 'getByIdTask' })),
  updateTask: updateTask(services, baseLogger.child({ controller: 'updateTask' })),
  getAllTask: getAllTask(services, baseLogger.child({ controller: 'getAllTask' })),
  createUser: createUser(services, baseLogger.child({ controller: 'createUser' }))
});
