####

# Challenge - TS & Clean Architecture boilerplate

## Description

Clean architecture offers benefits such as modularity, technology independence, testability, maintainability, code reusability, and scalability. It helps create more robust, flexible, and easily maintainable systems.

## Folders

```
application: contains the business logic
infraestructure: contains all the technologies and logic of the project
entities: definition of entities and interfaces that will be applied
use-cases: contains all the use cases your project needs
data-access: all external services that our project will implement as external apis or databases
services: the logic of our project
interfcaes: configuration of our server who is the one who receives and presents the information, middlewares, routes etc are configured
```

#### Run :

- install dependencies:

```
yarn install
```

- Run project:

```
yarn dev
```
