import Joi from 'joi';

export const configSchema = Joi.object({
  http: {
    port: Joi.string()
  },
  baseLogger: Joi.any(),
  mysql: {
    database: Joi.string(),
    username: Joi.string(),
    password: Joi.string().optional(),
    host: Joi.string(),
    dialect: Joi.string()
  }
});
