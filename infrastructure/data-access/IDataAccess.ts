import { IRepositoryTask } from '../../application/entities/task/Task.repository';
import { IRepositoryUser } from '../../application/entities/user/User.repository';

export type IDataAccess = {
  taskRepository: IRepositoryTask;
  userRepository: IRepositoryUser;
};
