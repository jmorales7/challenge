import { IServices } from 'infrastructure/services/IServices';
import { ILogger } from 'logger/Logger';

import { CustomError, Errors } from '../../../entities/shared/Errors';
import { CreateTaskDto } from '../../../entities/task/Task.dto';

type Payload = {
  body: CreateTaskDto;
  files: any;
};
export const createTask =
  ({ taskService, userService }: IServices, logger: ILogger) =>
  async ({ body, files }: Payload): Promise<any> => {
    if (body.shared) {
      const shared: string[] = JSON.parse(body.shared);
      const isValid = await userService.validateSharedIds(shared);
      if (!isValid) throw new CustomError(Errors.INVALID_PARAMETERS, 'Ids shared not exist');
    }
    if (files) {
      const file = files.find((f: any) => f.fieldname === 'file');
      const allowedExtensions = ['application/pdf', 'image/jpeg', 'image/png'];
      const fileExtension = file.mimetype;

      if (!allowedExtensions.includes(fileExtension))
        throw new CustomError(Errors.INVALID_PARAMETERS, `File ${fileExtension} is not allowed `);
      const fileName = file.originalname;
      //add logic to store in AWS S3 Or SFTP
      return await taskService.create({ file: fileName, ...body });
    }
    return await taskService.create(body);
  };
