import { IServices } from 'infrastructure/services/IServices';
import { ILogger } from 'logger/Logger';

export const deleteTask =
  ({ taskService }: IServices, logger: ILogger) =>
  async ({ id }: any): Promise<void> => {
    await taskService.delete(id);
  };
