import { IDataAccess } from 'infrastructure/data-access/IDataAccess';

import { ILogger } from '../../logger/Logger';
import { IServices } from './IServices';
import { taskService } from './task/service';
import { userService } from './user/service';

export const services = (dataAccess: IDataAccess, loggerFactory: ILogger, preInjection: boolean): IServices => {
  const logger = loggerFactory.child({ module: 'services' });
  logger.info('starting services');
  try {
    const taskServiceSetUp = taskService(dataAccess, logger.child({ service: 'task' }));
    const userServiceSetUp = userService(dataAccess, logger.child({ service: 'task' }));
    preInjection ? logger.info('Dependencies pre-injection done') : logger.info('Dependencies fully injected');
    return {
      taskService: taskServiceSetUp,
      userService: userServiceSetUp
    };
  } catch (e) {
    logger.error(e, 'failed on start services');
    throw e;
  }
};
