import { User } from './User';
import { CreateUserDto } from './User.dto';

export type IUser = {
  create(user: CreateUserDto): Promise<User>;
  validateSharedIds(shared: string[]): Promise<boolean>;
};
