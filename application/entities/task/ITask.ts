import { PaginatedResult, PaginationParams } from '../common/Pagination';
import { Task } from './Task';
import { CreateTaskDto, UpdateTaskDto } from './Task.dto';

export type ITask = {
  create(task: CreateTaskDto): Promise<Task>;
  getById(id: number): Promise<Task>;
  update(id: number, task: UpdateTaskDto): Promise<Task>;
  delete(id: string): Promise<void>;
  getAll(pagination: PaginationParams): Promise<PaginatedResult<Task>>;
};
