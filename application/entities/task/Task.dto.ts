import { z } from 'zod';

import { Status } from './Task';

export const CreateTaskDto = z.object({
  title: z.string(),
  description: z.string(),
  status: Status,
  dateToFinish: z.string(),
  isPublic: z.string(),
  shared: z.string().optional(),
  comment: z.string().optional(),
  createdBy: z.string(),
  responsible: z.string().optional(),
  tags: z.string().optional(),
  file: z.string().optional()
});

export type CreateTaskDto = z.infer<typeof CreateTaskDto>;

export const GetByIdTaskDto = z.object({
  id: z.number()
});
export type GetByIdTaskDto = z.infer<typeof GetByIdTaskDto>;

export const UpdateTaskDto = z.object({
  title: z.string().optional(),
  description: z.string().optional(),
  status: Status.optional(),
  dateToFinish: z.string().optional(),
  isPublic: z.boolean().optional(),
  comment: z.string().optional(),
  responsible: z.string().optional(),
  tags: z.string().optional(),
  file: z.string().optional(),
  userId: z.string()
});

export type UpdateTaskDto = z.infer<typeof UpdateTaskDto>;

export const GetAllTaskDto = z.object({
  limit: z.string().nonempty().regex(/^\d+$/, 'Invalid, need to be a number').transform(Number),
  page: z.string().nonempty().regex(/^\d+$/, 'Invalid, need to be a number').transform(Number),
  search: z.string().optional(),
  sort: z.string().optional()
});
export type GetAllTaskDto = z.infer<typeof GetAllTaskDto>;
