import { z } from 'zod';

export const CreateUserDto = z.object({
  name: z.string()
});
export type CreateUserDto = z.infer<typeof CreateUserDto>;
